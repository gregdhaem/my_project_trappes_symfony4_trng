<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    /**
    * @Route("/home", name="home")
    */
    public function index()
    {
        //       $number = random_int(1, 48);
        //       return $this->render('home/index.html.twig', [
        //           'controller_name' => 'HomeController',
        //           "user" => [
        //               "prenom" => "bob",
        //               "nom" => "l'éponge",
        //               'number' => $number,
        //               'avatar' => "img/bobleponge.jpg",
        //           ]
        //       ]);
        return $this->render('offline/signup.html.twig');  
    }
}
