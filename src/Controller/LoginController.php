<?php

namespace App\Controller;

use App\Entity\Users;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{

    public function login()
    {
        return new Response("Welcome to login page");
    }

    public function postregister(Request $request)
    {
        if (!filter_var($request->get("email"), FILTER_VALIDATE_EMAIL)) 
        {
            return $this->returnJson(array("path" => "/register", "Wrong Email syntax"), 401);
        }
        
        $er = $this->getDoctrine()->getRepository(Users::class);
        $oneUser = $er->findOneBy(["email" => $request->get("email")]);

        if (!$oneUser)
        {
            $em = $this->getDoctrine()->getManager();

            $user = new Users;
    
            $user->setName($request->get("first_name") . " " . $request->get("last_name"));
            $user->setEmail($request->get("email"));
            $user->setPassword( $this->encryptPassword($request->get("password")) );
            $user->setPhone($request->get("tel"));
            $user->setUpdated();        
            
            try 
            {
                $em->persist($user);
                $em->flush();
            } 
            catch (\Doctrine\ORM\EntityNotFoundException $e) 
            {
                return $this->returnJson(array("path" => "/register", "Invalid Request"), 500);  
            }
    
            if (true)
            {
                return $this->returnJson(array("path" => "/home", "New User Created"), 201);
            } 
            else 
            {
                return $this->returnJson(array("path" => "/register", "User not created"), 401);      
            }
        }
        else 
        {
            return $this->returnJson(array("path" => "/register", "User already exists"), 401); 
        }   
    }

    private function encryptPassword(string $password):string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    private function returnJson($data, $statusCode) 
    {
        return new Response(json_encode($data), $statusCode, array("Content-Type" => "application/json"));
    }
}
